package com.android.bachuchim.chitchat;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.bachuchim.chitchat.WiFiDirectApp.PTPLog;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * chat fragment attached to main activity.
 */
public class ChatFragment extends ListFragment {
	private static final String TAG = "PTP_ChatFrag";

    private int PICK_IMAGE_REQUEST = 1;
    private String imgS = "";
    private ImageView imgView;
    private FrameLayout imgFrame;
    private LinearLayout imgLayout;
	WiFiDirectApp mApp = null; 
	private static ChatActivity mActivity = null;
	
	private ArrayList<MessageRow> mMessageList = null;   // a list of chat msgs.
    private ArrayAdapter<MessageRow> mAdapter= null;
    
    // private String mMyAddr;
    
	/**
     * Static factory to create a fragment object from tab click.
     */
    public static ChatFragment newInstance(Activity activity, String groupOwnerAddr, String msg) {
    	ChatFragment f = new ChatFragment();
    	mActivity = (ChatActivity)activity;

        Bundle args = new Bundle();
        args.putString("groupOwnerAddr", groupOwnerAddr);
        args.putString("initMsg", msg);
        f.setArguments(args);
        Log.d(TAG, "newInstance :" + groupOwnerAddr + " : " + msg);
        return f;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {  // this callback invoked after newInstance done.
        super.onCreate(savedInstanceState);
        mApp = (WiFiDirectApp)mActivity.getApplication();

        setRetainInstance(true);   // Tell the framework to try to keep this fragment around during a configuration change.
    }
    
    /**
     * the data you place in the Bundle here will be available in the Bundle given to onCreate(Bundle), etc.
     * only works when your activity is destroyed by android platform. If the user closed the activity, no call of this.
     * http://www.eigo.co.uk/Managing-State-in-an-Android-Activity.aspx
     */
    @Override
    public void onSaveInstanceState(Bundle outState){
    	super.onSaveInstanceState(outState);
    	outState.putParcelableArrayList("MSG_LIST", mMessageList);
    	//Log.d(TAG, "onSaveInstanceState. " + mMessageList.get(0).mMsg);
    }
    
    /**
     * no matter your fragment is declared in main activity layout, or dynamically added thru fragment transaction
     * You need to inflate fragment view inside this function. 
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	// inflate the fragment's res layout. 
        final View contentView = inflater.inflate(R.layout.chat_frag, container, false);  // no care whatever container is.
        imgView = (ImageView) contentView.findViewById(R.id.loadImg);

        final EditText inputEditText = (EditText)contentView.findViewById(R.id.edit_input);

        imgFrame = (FrameLayout)contentView.findViewById(R.id.image_choose);
        imgLayout = (LinearLayout)contentView.findViewById(R.id.img_layout);
        final Button imgBtn = (Button)contentView.findViewById(R.id.btn_image);
        imgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgFrame.setVisibility(View.VISIBLE);
                imgLayout.setVisibility(View.GONE);
            }
        });

        final Button deleteImgBtn = (Button)contentView.findViewById(R.id.btn_delete_img);
        deleteImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgView.setImageDrawable(null) ;
                imgLayout.setVisibility(View.GONE);
            }
        });

        final Button closeImgFrame = (Button)contentView.findViewById(R.id.btn_close_img_frame);
        closeImgFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgFrame.setVisibility(View.GONE);
            }
        });

        final ImageButton btnIcon1 = (ImageButton) contentView.findViewById(R.id.btn_feel1);
        btnIcon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.icon1);
                getStringFromBitmap(icon);
                imgView.setImageBitmap(icon);
                Log.d(TAG, " lấy icon" );
                imgFrame.setVisibility(View.GONE);
                imgLayout.setVisibility(View.VISIBLE);
            }
        });

        final ImageButton btnIcon2 = (ImageButton) contentView.findViewById(R.id.btn_feel2);
        btnIcon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.icon2);
                getStringFromBitmap(icon);
                imgView.setImageBitmap(icon);
                Log.d(TAG, " lấy icon" );
                imgFrame.setVisibility(View.GONE);
                imgLayout.setVisibility(View.VISIBLE);
            }
        });

        final ImageButton btnIcon3 = (ImageButton) contentView.findViewById(R.id.btn_feel3);
        btnIcon3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.icon3);
                getStringFromBitmap(icon);
                imgView.setImageBitmap(icon);
                Log.d(TAG, " lấy icon" );
                imgFrame.setVisibility(View.GONE);
                imgLayout.setVisibility(View.VISIBLE);
            }
        });

        final ImageButton btnIcon4 = (ImageButton) contentView.findViewById(R.id.btn_feel4);
        btnIcon4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.icon4);
                getStringFromBitmap(icon);
                imgView.setImageBitmap(icon);
                Log.d(TAG, " lấy icon" );
                imgFrame.setVisibility(View.GONE);
                imgLayout.setVisibility(View.VISIBLE);
            }
        });

        final ImageButton btnIcon5 = (ImageButton) contentView.findViewById(R.id.btn_feel5);
        btnIcon5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.icon5);
                getStringFromBitmap(icon);
                imgView.setImageBitmap(icon);
                Log.d(TAG, " lấy icon" );
                imgFrame.setVisibility(View.GONE);
                imgLayout.setVisibility(View.VISIBLE);
            }
        });

        final ImageButton btnIcon6 = (ImageButton) contentView.findViewById(R.id.btn_feel6);
        btnIcon6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.icon6);
                getStringFromBitmap(icon);
                imgView.setImageBitmap(icon);
                Log.d(TAG, " lấy icon" );
                imgFrame.setVisibility(View.GONE);
                imgLayout.setVisibility(View.VISIBLE);
            }
        });

        final ImageButton btnIcon7 = (ImageButton) contentView.findViewById(R.id.btn_feel7);
        btnIcon7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.icon7);
                getStringFromBitmap(icon);
                imgView.setImageBitmap(icon);
                Log.d(TAG, " lấy icon" );
                imgFrame.setVisibility(View.GONE);
                imgLayout.setVisibility(View.VISIBLE);
            }
        });

        final ImageButton btnIcon8 = (ImageButton) contentView.findViewById(R.id.btn_feel8);
        btnIcon8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.icon8);
                getStringFromBitmap(icon);
                imgView.setImageBitmap(icon);
                Log.d(TAG, " lấy icon");
                imgFrame.setVisibility(View.GONE);
                imgLayout.setVisibility(View.VISIBLE);
            }
        });

        final ImageButton btnIcon9 = (ImageButton) contentView.findViewById(R.id.btn_feel9);
        btnIcon9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.icon9);
                getStringFromBitmap(icon);
                imgView.setImageBitmap(icon);
                Log.d(TAG, " lấy icon");
                imgFrame.setVisibility(View.GONE);
                imgLayout.setVisibility(View.VISIBLE);
            }
        });

        final ImageButton btnIcon10 = (ImageButton) contentView.findViewById(R.id.btn_feel10);
        btnIcon10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.icon10);
                getStringFromBitmap(icon);
                imgView.setImageBitmap(icon);
                Log.d(TAG, " lấy icon");
                imgFrame.setVisibility(View.GONE);
                imgLayout.setVisibility(View.VISIBLE);
            }
        });


        final Button imgBtn2 = (Button)contentView.findViewById(R.id.btn_image_from_gallery);
        imgBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // send the chat text in current line to the server
                Intent intent = new Intent();
                // Show only images, no videos or anything else
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
                PTPLog.d(TAG, "laasy hinh");
            }
        });

        //final EditText inputEditText = (EditText)contentView.findViewById(R.id.edit_input);
        final Button sendBtn = (Button)contentView.findViewById(R.id.btn_send);
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
				// send the chat text in current line to the server
				String inputMsg = inputEditText.getText().toString();
				inputEditText.setText("");
                imgView.setImageDrawable(null) ;
                imgLayout.setVisibility(View.GONE);
				InputMethodManager imm = (InputMethodManager)mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(inputEditText.getWindowToken(), 0);
				MessageRow row = new MessageRow(mApp.mDeviceName, inputMsg, null, imgS);
				appendChatMessage(row);
				String jsonMsg = mApp.shiftInsertMessage(row);
				PTPLog.d(TAG, "sendButton clicked: sendOut data : " + jsonMsg);
				mActivity.pushOutMessage(jsonMsg);
                imgS="";
			}
         });
        
        String groupOwnerAddr = getArguments().getString("groupOwnerAddr");
        String msg = getArguments().getString("initMsg");
        PTPLog.d(TAG, "onCreateView : fragment view created: msg :" + msg);
        
    	if( savedInstanceState != null ){
            mMessageList = savedInstanceState.getParcelableArrayList("MSG_LIST");
            Log.d(TAG, "onCreate : savedInstanceState: " + mMessageList.get(0).mMsg);
        }else if( mMessageList == null ){
        	// no need to setContentView, just setListAdapter, but listview must be android:id="@android:id/list"
            mMessageList = new ArrayList<MessageRow>();
            jsonArrayToList(mApp.mMessageArray, mMessageList);
            Log.d(TAG, "onCreate : jsonArrayToList : " + mMessageList.size() );
        }else {
        	Log.d(TAG, "onCreate : setRetainInstance good : ");
        }
        
        mAdapter = new ChatMessageAdapter(mActivity, mMessageList);
        
        setListAdapter(mAdapter);  // list fragment data adapter 
        
        PTPLog.d(TAG, "onCreate chat msg fragment: devicename : " + mApp.mDeviceName + " : " + getArguments().getString("initMsg"));
        return contentView;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
// Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public void getStringFromBitmap(Bitmap bitmap)
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 40, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        imgS = Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            Bitmap bitmap = null;
            try {
                // Get input stream of the image
                final BitmapFactory.Options options = new BitmapFactory.Options();
                // First decode with inJustDecodeBounds=true to check dimensions
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(uri),null, options);
                // Calculate inSampleSize
                int targetWidth = 640; // your arbitrary fixed limit
                int targetHeight = (int) (options.outHeight * targetWidth / (double) options.outWidth);
                options.inSampleSize = calculateInSampleSize(options,targetWidth , targetHeight);
                // Decode bitmap with inSampleSize set
                options.inJustDecodeBounds = false;
                bitmap=   BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(uri),null, options);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                imgS = Base64.encodeToString(byteArray, Base64.DEFAULT);
                imgView.setImageBitmap(bitmap);
                Log.d(TAG, " lấy hình" + imgS.length());
                imgFrame.setVisibility(View.GONE);
                imgLayout.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroyView(){ 
    	super.onDestroyView(); 
    	Log.d(TAG, "onDestroyView: ");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {  // invoked after fragment view created.
        super.onActivityCreated(savedInstanceState);
        
        if( mMessageList.size() > 0){
        	getListView().smoothScrollToPosition(mMessageList.size()-1);
        }
        
        setHasOptionsMenu(true);
        Log.d(TAG, "onActivityCreated: chat fragment displayed ");
    }
    
    /**
     * add a chat message to the list, return the format the message as " sender_addr : msg "
     */
    public void appendChatMessage(MessageRow row) {
    	Log.d(TAG, "appendChatMessage: chat fragment append msg: " + row.mSender + " ; " + row.mMsg);
    	mMessageList.add(row);
    	getListView().smoothScrollToPosition(mMessageList.size()-1);
    	mAdapter.notifyDataSetChanged();  // notify the attached observer and views to refresh.
    	return;
    }
    
    private void jsonArrayToList(JSONArray jsonarray, List<MessageRow> list) {
    	try{
    		for(int i=0;i<jsonarray.length();i++){
    			MessageRow row = MessageRow.parseMesssageRow(jsonarray.getJSONObject(i));
    			PTPLog.d(TAG, "jsonArrayToList: row : " + row.mMsg);
    			list.add(row);
    		}
    	}catch(JSONException e){
    		PTPLog.e(TAG, "jsonArrayToList: " + e.toString());
    	}
    }
    
    /**
     * chat message adapter from list adapter.
     * Responsible for how to show data to list fragment list view.
     */
    final class ChatMessageAdapter extends ArrayAdapter<MessageRow> {

    	public static final int VIEW_TYPE_MYMSG = 0;
    	public static final int VIEW_TYPE_INMSG = 1;
    	public static final int VIEW_TYPE_COUNT = 2;    // msg sent by me, or all incoming msgs
    	private LayoutInflater mInflater;
    	
		public ChatMessageAdapter(Context context, List<MessageRow> objects){
			super(context, 0, objects);
            mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
		
		@Override
        public int getViewTypeCount() {
            return VIEW_TYPE_COUNT;
        }
		
		@Override
        public int getItemViewType(int position) {
			MessageRow item = this.getItem(position);
			if ( item.mSender.equals(mApp.mDeviceName )){
				return VIEW_TYPE_MYMSG;
			}
			return VIEW_TYPE_INMSG;			
		}
		
		/**
		 * assemble each row view in the list view.
		 * http://dl.google.com/googleio/2010/android-world-of-listview-android.pdf
		 */
		@Override
        public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;  // old view to re-use if possible. Useful for Heterogeneous list with diff item view type.
			MessageRow item = this.getItem(position);
			boolean mymsg = false;
			
			if ( getItemViewType(position) == VIEW_TYPE_MYMSG){
				if( view == null ){
	            	view = mInflater.inflate(R.layout.chat_row_mymsg, null);  // inflate chat row as list view row.
	            }
				mymsg = true;
				// view.setBackgroundResource(R.color.my_msg_background);
			} else {
				if( view == null ){
	            	view = mInflater.inflate(R.layout.chat_row_inmsg, null);  // inflate chat row as list view row.
	            }
				// view.setBackgroundResource(R.color.in_msg_background);
			}
			
            TextView sender = (TextView)view.findViewById(R.id.sender);
            sender.setText(item.mSender);
            
            TextView msgRow = (TextView)view.findViewById(R.id.msg_row);
            msgRow.setText(item.mMsg);
            if (msgRow.getLineCount()==1) msgRow.setText(item.mMsg + "\n ");
            if( mymsg ){
            	msgRow.setBackgroundResource(R.color.my_msg_background);	
            }else{
            	msgRow.setBackgroundResource(R.color.in_msg_background);
            }
            
            TextView time = (TextView)view.findViewById(R.id.time);
            time.setText(item.mTime);

            ImageView image = (ImageView) view.findViewById(R.id.photo);
            image.setImageDrawable(null) ;
           if (!item.mImg.equals(""))
            {
                Log.d(TAG, "VVVVV" + item.mSender +item.mImg.length());
            String imageDataBytes = item.mImg.substring(item.mImg.indexOf(",")+1);
            InputStream stream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT));
            Bitmap bit = BitmapFactory.decodeStream(stream);
            image.setImageBitmap(bit);}




            Log.d(TAG, "getView : " + item.mSender + " " + item.mMsg + " " + item.mTime);
            return view;
		}
    }
}
