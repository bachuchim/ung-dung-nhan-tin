/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.bachuchim.chitchat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.android.bachuchim.chitchat.DeviceListFragment.DeviceActionListener;
import com.android.bachuchim.chitchat.WiFiDirectApp.PTPLog;

/**
 * An activity that uses WiFi Direct APIs to discover and connect with available
 * devices. WiFi Direct APIs are asynchronous and rely on callback mechanism
 * using interfaces to notify the application of operation success or failure.
 * The application should also register a BroadcastReceiver for notification of
 * WiFi state related events.
 */
public class WiFiDirectActivity extends AppCompatActivity implements DeviceActionListener {

    public static final String TAG = "PTP_Activity";

    WiFiDirectApp mApp = null;
    private int PICK_IMAGE_REQUEST = 1;
    protected static final int CHOOSE_FILE_RESULT_CODE = 20;
    boolean mHasFocus = false;
    private boolean retryChannel = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);   // statically draw two <fragment class=>
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mApp = (WiFiDirectApp)getApplication();

        mApp.mHomeActivity = this;

        // If service not started yet, start it.
        Intent serviceIntent = new Intent(this, ConnectionService.class);
        startService(serviceIntent);  // start the connection service

        String color =  AppPreferences.getStringFromPref(this, AppPreferences.PREF_NAME, AppPreferences.APP_BGR);
        if (color!=null && !color.equals("")) {
            color = color.trim();
            LinearLayout ll = (LinearLayout) findViewById(R.id.mainLayout);
            ll.setBackgroundColor(Color.parseColor(color));
            Log.d(TAG, " set background color");
        }
        PTPLog.d(TAG, "onCreate : home activity launched, start service anyway.");
    }

    /**
     *
     */
    @Override
    public void onResume() {
        super.onResume();
        mHasFocus = true;
        if( mApp.mThisDevice != null ){
        	PTPLog.d(TAG, "onResume : redraw this device details");
        	updateThisDevice(mApp.mThisDevice);

        	// if p2p connetion info available, and my status is connected, enabled start chatting !
            if( mApp.mP2pInfo != null && mApp.mThisDevice.status == WifiP2pDevice.CONNECTED){
            	PTPLog.d(TAG, "onResume : redraw detail fragment");
            	onConnectionInfoAvailable(mApp.mP2pInfo);
            } else {
            	// XXX stop client, if any.
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

	@Override
	public void onStop() {  // the activity is no long visible
		super.onStop();
		mHasFocus = false;
	}

    @Override
    public void onDestroy() {
    	super.onDestroy();
    	mApp.mHomeActivity = null;
    	PTPLog.d(TAG, "onDestroy: reset app home activity.");
    }

    /**
     * Remove all peers and clear all fields. This is called on
     * BroadcastReceiver receiving a state change event.
     */
    public void resetData() {
    	runOnUiThread(new Runnable() {
    		@Override
            public void run() {
    			DeviceListFragment fragmentList = (DeviceListFragment) getFragmentManager().findFragmentById(R.id.frag_list);
    			DeviceDetailFragment fragmentDetails = (DeviceDetailFragment) getFragmentManager().findFragmentById(R.id.frag_detail);
    			if (fragmentList != null) {
    				fragmentList.clearPeers();
    			}
    			if (fragmentDetails != null) {
    				fragmentDetails.resetViews();
    			}
    		}
    	});
    }

    /**
     * process WIFI_P2P_THIS_DEVICE_CHANGED_ACTION intent, refresh this device.
     */
    public void updateThisDevice(final WifiP2pDevice device){
    	runOnUiThread(new Runnable() {
    		@Override
            public void run() {
    			DeviceListFragment fragment = (DeviceListFragment)getFragmentManager().findFragmentById(R.id.frag_list);
    			fragment.updateThisDevice(device);
    		}
    	});
    }

    /**
     * update the device list fragment.
     */
    public void onPeersAvailable(final WifiP2pDeviceList peerList){
    	runOnUiThread(new Runnable() {
    		@Override
            public void run() {
    			DeviceListFragment fragmentList = (DeviceListFragment) getFragmentManager().findFragmentById(R.id.frag_list);
    	    	fragmentList.onPeersAvailable(mApp.mPeers);  // use application cached list.
    	    	DeviceDetailFragment fragmentDetails = (DeviceDetailFragment) getFragmentManager().findFragmentById(R.id.frag_detail);

    	    	for(WifiP2pDevice d : peerList.getDeviceList()){
    	    		if( d.status == WifiP2pDevice.FAILED ){
    	    			PTPLog.d(TAG, "onPeersAvailable: Peer status is failed " + d.deviceName );
    	    	    	fragmentDetails.resetViews();
    	    		}
    	    	}
    		}
    	});
    }

    /**
     * handle p2p connection available, update UI.
     */
    public void onConnectionInfoAvailable(final WifiP2pInfo info) {
    	runOnUiThread(new Runnable() {
    		@Override
            public void run() {
    			DeviceDetailFragment fragmentDetails = (DeviceDetailFragment) getFragmentManager().findFragmentById(R.id.frag_detail);
    			fragmentDetails.onConnectionInfoAvailable(info);
    		}
    	});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_items, menu);

        MenuItem itemSwitch = menu.findItem(R.id.switch_notification);
        itemSwitch.setActionView(R.layout.switch_item);
        final Switch sw = (Switch) menu.findItem(R.id.switch_notification).getActionView().findViewById(R.id.action_switch);



        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    AppPreferences.setStringToPref(mApp, AppPreferences.PREF_NAME, AppPreferences.APP_NOTI, "on");
                    Log.d(TAG, " on notification");
                }
                else
                {
                    AppPreferences.setStringToPref(mApp, AppPreferences.PREF_NAME, AppPreferences.APP_NOTI, "off");
                    Log.d(TAG, " off otification");
                }
            }
        });

        String noti =  AppPreferences.getStringFromPref(this, AppPreferences.PREF_NAME, AppPreferences.APP_NOTI);
        if (noti!=null && "off".equals(noti.trim())){
            sw.setChecked(false);
        }
        else {
            sw.setChecked(true);
        }
        return true;
    }

    /**
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        	case android.R.id.home:   // using app icon for navigation up or home:
        		Log.d(TAG, " navigating up or home clicked.");
        		// startActivity(new Intent(home.class, Intent.FLAG_ACTIVITY_CLEAR_TOP));
        		return true;

            case R.id.atn_direct_discover:
               /* if( !mApp.isP2pEnabled() ){
                    Toast.makeText(WiFiDirectActivity.this, R.string.p2p_off_warning, Toast.LENGTH_LONG).show();
                    return true;
                }*/

                // show progressbar when discoverying.
                final DeviceListFragment fragment = (DeviceListFragment) getFragmentManager().findFragmentById(R.id.frag_list);
                fragment.onInitiateDiscovery();

                PTPLog.d(TAG, "onOptionsItemSelected : start discoverying ");
                mApp.mP2pMan.discoverPeers(mApp.mP2pChannel, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(WiFiDirectActivity.this, "Thiết lập tìm kiếm", Toast.LENGTH_SHORT).show();
                        PTPLog.d(TAG, "onOptionsItemSelected : discovery succeed... " );
                    }

                    @Override
                    public void onFailure(int reasonCode) {
                    	PTPLog.d(TAG, "onOptionsItemSelected : discovery failed !!! " + reasonCode);
                    	fragment.clearPeers();
                        Toast.makeText(WiFiDirectActivity.this, "Tìm kiếm thất bại, hãy thử lại... ", Toast.LENGTH_SHORT).show();
                    }
                });
                return true;
            case R.id.bg_blue:{
                AppPreferences.setStringToPref(mApp, AppPreferences.PREF_NAME, AppPreferences.APP_BGR, "#cce6ff");
                LinearLayout ll = (LinearLayout) findViewById(R.id.mainLayout);
                ll.setBackgroundColor(Color.parseColor("#cce6ff"));
                Log.d(TAG, " set background color blue");
                return true;}

            case R.id.bg_yellow:{
                AppPreferences.setStringToPref(mApp, AppPreferences.PREF_NAME, AppPreferences.APP_BGR, "#ffffcc");
                LinearLayout ll = (LinearLayout) findViewById(R.id.mainLayout);
                ll.setBackgroundColor(Color.parseColor("#ffffcc"));
                Log.d(TAG, " set background color yellow");
                return true;}

            case R.id.bg_white:{
                AppPreferences.setStringToPref(mApp, AppPreferences.PREF_NAME, AppPreferences.APP_BGR, "#ffffff");
                LinearLayout ll = (LinearLayout) findViewById(R.id.mainLayout);
                ll.setBackgroundColor(Color.parseColor("#ffffff"));
                Log.d(TAG, " set background color white");
                return true;}

            case R.id.bg_orange:{
                AppPreferences.setStringToPref(mApp, AppPreferences.PREF_NAME, AppPreferences.APP_BGR, "#ff944d");
                LinearLayout ll = (LinearLayout) findViewById(R.id.mainLayout);
                ll.setBackgroundColor(Color.parseColor("#ff944d"));
                Log.d(TAG, " set background color orange");
                return true;}

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * user taps on peer from discovered list of peers, show this peer's detail.
     */
    public void showDetails(WifiP2pDevice device) {
        DeviceDetailFragment fragment = (DeviceDetailFragment) getFragmentManager().findFragmentById(R.id.frag_detail);
        fragment.showDetails(device);
    }

    /**
     * user clicked connect button after discover peers.
     */
    public void connect(WifiP2pConfig config) {
    	PTPLog.d(TAG, "connect : connect to server : " + config.deviceAddress);
    	// perform p2p connect upon users click the connect button. after connection, manager request connection info.
        mApp.mP2pMan.connect(mApp.mP2pChannel, config, new ActionListener() {
            @Override
            public void onSuccess() {
                // WiFiDirectBroadcastReceiver will notify us. Ignore for now.
            	Toast.makeText(WiFiDirectActivity.this, "Kết nối thành công..", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int reason) {
                Toast.makeText(WiFiDirectActivity.this, "Kết nối thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * user clicked disconnect button, disconnect from group owner.
     */
    public void disconnect() {
        final DeviceDetailFragment fragment = (DeviceDetailFragment) getFragmentManager().findFragmentById(R.id.frag_detail);
        fragment.resetViews();
        PTPLog.d(TAG, "disconnect : removeGroup " );
        mApp.mP2pMan.removeGroup(mApp.mP2pChannel, new ActionListener() {
            @Override
            public void onFailure(int reasonCode) {
                PTPLog.d(TAG, "Disconnect failed. Reason : 1=error, 2=busy; " + reasonCode);
                Toast.makeText(WiFiDirectActivity.this, "Hủy kết nối thất bại.." + reasonCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess() {
            	PTPLog.d(TAG, "Disconnect succeed. ");
                fragment.getView().setVisibility(View.GONE);
            }
        });
    }

    /**
     * The channel to the framework(WiFi direct) has been disconnected.
     * This is diff than the p2p connection to group owner.
     */
    public void onChannelDisconnected() {
        Toast.makeText(this, "Mất kết nối. Bật lại wifi direct", Toast.LENGTH_LONG).show();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("WiFi Direct down, please re-enable WiFi Direct")
	       .setCancelable(true)
	       .setPositiveButton("Re-enable WiFi Direct", new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	        	   startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
	           }
	       })
	       .setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	                finish();
	           }
	       });

        AlertDialog info = builder.create();
        info.show();
    }

    @Override
    public void cancelDisconnect() {
        /*
         * A cancel abort request by user. Disconnect i.e. removeGroup if
         * already connected. Else, request WifiP2pManager to abort the ongoing
         * request
         */
        if (mApp.mP2pMan != null) {
            final DeviceListFragment fragment = (DeviceListFragment) getFragmentManager().findFragmentById(R.id.frag_list);
            if (fragment.getDevice() == null || fragment.getDevice().status == WifiP2pDevice.CONNECTED) {
                disconnect();
            } else if (fragment.getDevice().status == WifiP2pDevice.AVAILABLE || fragment.getDevice().status == WifiP2pDevice.INVITED) {
                mApp.mP2pMan.cancelConnect(mApp.mP2pChannel, new ActionListener() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(WiFiDirectActivity.this, "Đang từ chối kết nối", Toast.LENGTH_SHORT).show();
                        PTPLog.d(TAG, "cancelConnect : success canceled...");
                    }
                    @Override
                    public void onFailure(int reasonCode) {
                        Toast.makeText(WiFiDirectActivity.this, "thoát kết nối thất bại, hãy thử lại.. ", Toast.LENGTH_SHORT).show();
                        PTPLog.d(TAG, "cancelConnect : cancel connect request failed..." + reasonCode);
                    }
                });
            }
        }
    }

    /**
     * launch chat activity
     */
    public void startChatActivity(final String initMsg) {
    	if( ! mApp.mP2pConnected ){
    		Log.d(TAG, "startChatActivity : p2p connection is missing, do nothng...");
    		return;
    	}

    	PTPLog.d(TAG, "startChatActivity : start chat activity fragment..." + initMsg);
    	runOnUiThread(new Runnable() {
    		@Override
            public void run() {
    			Intent i = mApp.getLauchActivityIntent(ChatActivity.class, initMsg);
    	    	startActivity(i);
    		}
    	});
    }
}
